class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this._salary = salary;
  }
  get name() {
    return this.name;
  }
  set name(name) {
    return (this._name = name);
  }
  get age() {
    return this.age;
  }
  set age(age) {
    return (this._age = age);
  }
  get salary() {
    return this.salary;
  }
  set salary(salary) {
    return (this._salary = salary);
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
}

let employee = new Employee("Mari", "27", 1000);
console.log(employee);

let programmer = new Programmer("Stepan", "30", 1500, "C++");
console.log(programmer);

let programmer2 = new Programmer("Oleg", "33", 2500, "Python");
console.log(programmer2);

let programmer3 = new Programmer("Gennadiy", "45", 5000, "JS");
console.log(programmer3);
console.log(programmer3.salary);
